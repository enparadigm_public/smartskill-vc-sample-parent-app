package com.enparadigm.test;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.enparadigm.init.SmartSkill;
import com.enparadigm.init.SmartSkillConfig;

public class MyApp extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(SmartSkill.setBaseContext(base));
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SmartSkillConfig config = new SmartSkillConfig.Builder("https://dev.enparadigm.com/settlein_devapi/public/index.php/v1/")
                .build();
        SmartSkill.init(this, config);

    }
}
