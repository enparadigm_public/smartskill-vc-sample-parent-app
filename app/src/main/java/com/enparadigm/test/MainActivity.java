package com.enparadigm.test;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.enparadigm.init.GetLanguageCallback;
import com.enparadigm.init.SmartSkill;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity {


    Context context;

    ProgressBar progressBar;
    Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progressBar);
        btnStart = findViewById(R.id.btn_start);
        context = this;

        FirebaseMessaging.getInstance().subscribeToTopic("9999999999");
        btnStart.setOnClickListener(view -> {
            progressBar.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.GONE);
            String json = "";
            if (SmartSkill.canLaunchSDK(json)) {
                progressBar.setVisibility(View.GONE);
                btnStart.setVisibility(View.VISIBLE);
                SmartSkill.start(context);
                finish();
            } else {
                String accessToken = "hjCpn0TtRy1q7OOUtnC77KzDO";
                String refreshToken = "aePK3eYedi49SeQWKgJEfQeMs";
                String displayName = "Tester";
                String languageShortForm = "en";
                String uniqueId = "VFgOqMQca9999999999";
                SmartSkill.setPluginData(accessToken, refreshToken, displayName, languageShortForm, uniqueId, json, new GetLanguageCallback() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                        btnStart.setVisibility(View.VISIBLE);
                        SmartSkill.start(context);
                        finish();
                    }

                    @Override
                    public void onInvalidLanguage() {
                        progressBar.setVisibility(View.GONE);
                        btnStart.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Selected language not available.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(String s) {
                        progressBar.setVisibility(View.GONE);
                        btnStart.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "onFailure : " + s, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}