package com.enparadigm.test;

import android.util.Log;

import com.enparadigm.init.SmartSkill;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseMessageService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("FirebaseMessageService", " onMessageReceived");
        remoteMessage.getData().put("smartskill_sdk","true");
        SmartSkill.handleFcmNotification(getApplicationContext(), remoteMessage);
    }

    @Override
    public void onNewToken(String s) {
        Log.d("FirebaseMessageService", " Token : " + s);
        SmartSkill.saveFcmToken(s);
        FirebaseMessaging.getInstance().subscribeToTopic("9999999999");
        super.onNewToken(s);
    }
}
